// LAN modul W5500 - web server

#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <Adafruit_LPS35HW.h>

Adafruit_LPS35HW lps35hw = Adafruit_LPS35HW();

// Set up MAC adress
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
// Set up ip adress. Must be in range of dhcp
IPAddress ip(192, 168, 1, 50);
// init web server on port 80
//EthernetServer server(80);

#define SLEEPTIME 600000 //ms

// pinout for Trig and Echo of ultrasonic module
int pTrig = 4;
int pEcho = 5;
// init vars
double odezva;
double vzdalenost;
#define pPump 6
#define pPump2 7


//mqtt config
#define mqtt_server "192.168.0.1"
#define mqtt_user "user"
#define mqtt_password "password"
#define mqtt_wtopic "sensor/waterlevel"
#define mqtt_wtemp "sensor/wtemp"
#define mqtt_wpress "sensor/wpressure"
//#define mqtt_send_interval 12000 //ms TODO x50
#define mqtt_send_interval 600000 //ms

#define mqtt_sub_topic_pump "controll/pump"
#define pumpON LOW
#define pumpOFF HIGH

//pump timer
#define pump_max_time 600000 //10 minutes
//const unsigned long pump_max_time = (6000 * 10) //10 minutes
static unsigned long pump_last_impulse = 0;
static int pumpon = false;

//Pressure sensor
// For SPI mode, we need a CS pin
//#define LPS_CS  10
// For software-SPI mode we need SCK/MOSI/MISO pins
//#define LPS_SCK  13
//#define LPS_MISO 12
//#define LPS_MOSI 11

//MQtt
EthernetClient ethClient;
PubSubClient client(ethClient);

void setupPump() {
  pinMode(pPump, OUTPUT);
  digitalWrite(pPump, pumpOFF);
}


void setpump(byte* onoff) {
  int ival = atoi(onoff);
  Serial.print("Recvd: ");
  Serial.println(ival);
  if (ival == 1 ) {
    digitalWrite(pPump, pumpON);
    pump_last_impulse = millis();
    pumpon = true;
    Serial.println("Pump on");
  }
  else {
    digitalWrite(pPump, pumpOFF);
    pump_last_impulse = millis();
    pumpon = false;
    Serial.println("Pump off");
  }
}

void checkpump() {
  //  Serial.print(millis() - pump_last_impulse);
  //  Serial.print(" > ");
  //  Serial.println(pump_max_time);
  if (pumpon && (millis() - pump_last_impulse) > pump_max_time) {
    Serial.println("Fallback stop pump!");
    digitalWrite(pPump, pumpOFF);
    pumpon = false;
  }
}

void mqttcallback(char* topic, byte* payload, unsigned int length) {
  //Serial.println(topic);
  if (strcmp(topic, mqtt_sub_topic_pump) == 0) {
    setpump(payload);
  }
}

void setup_preasure() {
  //  Serial.begin(115200);
  // Wait until serial port is opened
  //  while (!Serial) { delay(1); }

  Serial.println("Adafruit LPS35HW Test");

  if (!lps35hw.begin_I2C()) {
    //if (!lps35hw.begin_SPI(LPS_CS)) {
    //if (!lps35hw.begin_SPI(LPS_CS, LPS_SCK, LPS_MISO, LPS_MOSI)) {
    Serial.println("Couldn't find LPS35HW chip");
    //while (1);
  }
  lps35hw.setDataRate(LPS35HW_RATE_ONE_SHOT);
  Serial.println("Found LPS35HW chip");
}


void setup_hc() {
  // Set up pins of  HC-SR04
  pinMode(pTrig, OUTPUT);
  pinMode(pEcho, INPUT);
}

void reconnect_mqtt() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    // If you do not want to use a username and password, change next line to
    // if (client.connect("ESP8266Client")) {
    if (client.connect("kebule", mqtt_user, mqtt_password)) {
      client.subscribe(mqtt_sub_topic_pump);
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void ipsetup() {
  static int started = false;
  //Serial.println("--Link status: ");
  //Serial.println(Ethernet.linkStatus());
  if (started) {
    Ethernet.maintain();
    if (Ethernet.linkStatus() == LinkON) {
      return;
    }
  }

  Ethernet.begin(mac);
  // Allow hw to sert it out
  delay(1500);
  //server.begin();
  // Serial out debug
  Serial.print("Server je na IP adrese: ");
  Serial.println(Ethernet.localIP());
  started = true;
  Serial.print("xLink status: ");
  Serial.println(Ethernet.linkStatus());
  //mqtt
  client.setServer(mqtt_server, 1883);
  client.setCallback(mqttcallback);

  reconnect_mqtt();
}

bool checkBound(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

void setup() {
  // init serial line 9600 baud
  Serial.begin(9600);
  // Setup ip layer
  //Ethernet.begin(mac, ip);
  ipsetup();
  setup_hc();
  setupPump();
  setup_preasure();
  pinMode(LED_BUILTIN, OUTPUT);
}


double getdistance2()
{
  int tries = 5;

  while (tries > 0) {
    digitalWrite(pTrig, HIGH);              //begin to send a high pulse, then US-020 begin to measure the distance
    delayMicroseconds(50);                    //set this high pulse width as 50us (>10us)
    digitalWrite(pTrig, LOW);               //end this high pulse

    odezva = pulseIn(pEcho, HIGH);               //calculate the pulse width at EchoPin,
    if ((odezva < 60000) && (odezva > 1))    //a valid pulse width should be between (1, 60000).
    {
      vzdalenost = (odezva * 34 / 100) / 20.0; //calculate the distance by pulse width, Len_mm = (Time_Echo_us * 0.34mm/us) / 2 (mm)
      Serial.print("Echooo is: ");
      Serial.print(odezva);
      Serial.print("Present Distance is: ");  //output result to Serial monitor
      Serial.print(vzdalenost, DEC);            //output result to Serial monitor
      Serial.println("mm");                 //output result to Serial monitor
      return vzdalenost;
    }
    delayMicroseconds(200);
    tries--;
  }
  //delay(1000);                            //take a measurement every second (1000ms)
  return 0.0;
}

double getdistance()
{
  // Set 2us out to GND (for safety)
  // Then set 5us output to HIGH
  // Then LOW again
  digitalWrite(pTrig, LOW);
  delayMicroseconds(2);
  digitalWrite(pTrig, HIGH);
  delayMicroseconds(5);
  digitalWrite(pTrig, LOW);
  // By pulseIn get length of pulse in us
  odezva = pulseIn(pEcho, HIGH);
  //Serial.println("Odezva");
  //Serial.println(odezva);
  //Serial.println("s");

  // Calculate distance from time
  //vzdalenost = odezva / 58.31;
  vzdalenost = odezva * 0.034 / 2.0;
  //  Serial.print("Vzdalenost je ");
  //  Serial.print(vzdalenost);
  //  Serial.println(" cm.");
  // pauza 0.5 s pro přehledné čtení
  return vzdalenost;
}

double get_pressure() {
  lps35hw.takeMeasurement();
  double val = lps35hw.readPressure();
  Serial.print("Pressure: ");
  Serial.print(val);
  Serial.println(" hPa");
  return val;
}

double get_temperature() {
  lps35hw.takeMeasurement();
  double val = lps35hw.readTemperature();
  Serial.print("Temperature: ");
  Serial.print(val);
  Serial.println(" C");
  return val;
}

void send_data() {
  unsigned long currentMillis = millis(); // refresh counter variable
  static unsigned long lastSent = 0 - mqtt_send_interval;
  double val;

  if (currentMillis - lastSent >= mqtt_send_interval) {
    char c_topic[100];
    double dist = 190.0 - getdistance2();
    Serial.print("distance sent>: ");
    Serial.print(dist);
    Serial.println(" cm.");
    Serial.print(dist);
    if (dist < 189.0) {
      String topic = mqtt_wtopic;
      topic.toCharArray(c_topic, 100);
      client.publish(c_topic, String(dist).c_str(), true);
      lastSent = currentMillis;
      Serial.print("distance sent<: ");
      Serial.print(dist);
      Serial.println(" cm.");
    }
    //Pressure
    val = get_pressure();
    String topic = mqtt_wpress;
    topic.toCharArray(c_topic, 100);
    client.publish(c_topic, String(val).c_str(), true);
    //temperature
    val = get_temperature();
    topic = mqtt_wtemp;
    topic.toCharArray(c_topic, 100);
    client.publish(c_topic, String(val).c_str(), true);

  }
}

void blink() {
  static int led = LOW;

  // Change with every call
  if (led == LOW)
    led = HIGH;
  else
    led = LOW;

  //digitalWrite(LED_BUILTIN, led);
  digitalWrite(LED_BUILTIN, HIGH);
  delay(250);
  digitalWrite(LED_BUILTIN, LOW);
  delay(250);
}

void loop() {
  ipsetup();
  // Send data
  send_data();
  //delay(3000);
  client.loop();
  checkpump();
  reconnect_mqtt();
  //blink();
  delay(500);
}
